package com.msvc.usuario.controllers;

import com.msvc.usuario.entities.Usuario;
import com.msvc.usuario.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService UsuarioService;

    @PostMapping
    public ResponseEntity<Usuario> guardarUsuario(@RequestBody Usuario usuario) {
        Usuario usu = UsuarioService.saveUsuario(usuario);
        return ResponseEntity.status(HttpStatus.CREATED).body(usu);
    }

    @GetMapping("/{usuarioId}")
    public ResponseEntity<Usuario> obtenerUsuario(@PathVariable String usuarioId) {
        Usuario usu = UsuarioService.getUsuario(usuarioId);
        return ResponseEntity.ok(usu);
    }

    @GetMapping
    public ResponseEntity<List<Usuario>> obtenerTodosUsuario() {
        List<Usuario> usuarios = UsuarioService.getAllUsuarios();
        return ResponseEntity.ok(usuarios);
    }
}
