package com.msvc.usuario.services.impl;

import com.msvc.usuario.entities.Calificacion;
import com.msvc.usuario.entities.Hotel;
import com.msvc.usuario.entities.Usuario;
import com.msvc.usuario.exceptions.ResourceNotFoundException;
import com.msvc.usuario.external.services.HotelService;
import com.msvc.usuario.repository.UsuarioRepository;
import com.msvc.usuario.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    private Logger logger = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private HotelService hotelService;

    @Override
    public Usuario saveUsuario(Usuario usuario) {
        String usuarioId = UUID.randomUUID().toString();
        usuario.setUsuarioId(usuarioId);
        return usuarioRepository.save(usuario);
    }

    @Override
    public List<Usuario> getAllUsuarios() {
        return usuarioRepository.findAll();
    }

    @Override
    public Usuario getUsuario(String usuarioId) {

        Usuario usuario = usuarioRepository.findById(usuarioId)
                .orElseThrow(() -> new ResourceNotFoundException("Usuario no encontrado"));

        // String route = "http://localhost:8083/calificaciones/usuario/" + usuario.getUsuarioId();
        String route = "http://CALIFICACION-SERVICE/calificaciones/usuario/" + usuario.getUsuarioId();
        Calificacion[] calificacionesDelUsuario = restTemplate.getForObject(route, Calificacion[].class);

        List<Calificacion> calificaciones = Arrays.stream(calificacionesDelUsuario)
                .collect(Collectors.toList());

        List<Calificacion> listCalificaciones = calificaciones.stream().map(calificacion -> {
            System.out.println("ID: " + calificacion.getHotelId());
            String routeH = "http://HOTEL-SERVICE/hoteles/" + calificacion.getHotelId();
            //String routeH = "http://localhost:8082/hoteles/" + calificacion.getHotelId();
            //ResponseEntity<Hotel> forEntity = restTemplate.getForEntity(routeH, Hotel.class);
            //Hotel hotel = forEntity.getBody();
            Hotel hotel = hotelService.getHotel(calificacion.getHotelId());
            //logger.info("Respuesta con codigo de estado: {}", forEntity.getStatusCode());
            calificacion.setHotel(hotel);
            return calificacion;
        }).collect(Collectors.toList());

        usuario.setCalificaciones(listCalificaciones);

        return usuario;
    }
}
